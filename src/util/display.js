export function timeString( t ) {

	if ( !t ) return '';

	let s = '';
	var hours = Math.floor( t / 3600 );
	if ( hours > 0 ) {
		s += hours + 'h ';
		t -= 3600*hours;
	}
	var mins = Math.floor( t / 60 );
	if ( mins > 0 ) {
		s += mins + 'm ';
		t -= 60*mins;
	}
	s += t + 's';

	return s;

}